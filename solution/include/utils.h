//
// Created by Полина Калугина on 11.01.2024.
//
#include <stdint.h>

#include "bmp.h"
#include "image.h"



#ifndef ASSIGNMENT_IMAGE_ROTATIONFINAL_UTILS_H
#define ASSIGNMENT_IMAGE_ROTATIONFINAL_UTILS_H

uint32_t cal_padding(uint32_t width);

void free_img(struct image img);

#endif //ASSIGNMENT_IMAGE_ROTATIONFINAL_UTILS_H
