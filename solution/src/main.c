#include <stdio.h>

#include "deserializer.h"
#include "rotate.h"
#include "serializer.h"

#define ARGC_COUT 3                     //количество аргументов, переданных программе
#define PROGRAM_NAME argv[0]            // название запущенного файла
#define SOURCE_IMAGE argv[1]            //входной файл
#define TRANSFORMED_IMAGE argv[2]       //выходной файл

int main(int argc, char** argv) {
    if (argc != ARGC_COUT) {
        printf("Usage: %s <source-image> <transformed-image>\n", PROGRAM_NAME);
        return 1;
    }

    FILE *in = fopen(SOURCE_IMAGE, "rb");
    if (in == NULL) {
        printf("Error opening source image file\n");
        return 1;
    }

    FILE *out = fopen(TRANSFORMED_IMAGE, "wb");
    if (out == NULL) {
        printf("Error opening transformed image file\n");
        fclose(in);
        return 1;
    }

    struct image img = {0};
    enum read_status status = from_bmp(in, &img);
    if (status != READ_OK) {
        printf("Error reading source image: %d\n", status);
        fclose(in);
        fclose(out);
        free_img(img);
        return 1;
    }



    struct image transformed_img = rotate(img);
    free_img(img);

    enum write_status write_status = to_bmp(out, &transformed_img);

    free_img(transformed_img);
    fclose(in);
    fclose(out);

    if (write_status != WRITE_OK) {
        printf("Error writing transformed image: %d\n", write_status);

        return 1;
    }


    return 0;
}


