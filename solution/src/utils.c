//
// Created by Полина Калугина on 11.01.2024.
//
#include "utils.h"

#include <stdlib.h>

#define IMG_ALIGN 4

uint32_t cal_padding(uint32_t width)
{
    return (IMG_ALIGN - (width * PIXEL_SIZE) % IMG_ALIGN) % IMG_ALIGN;
}

void free_img(struct image img){
    if(img.data != NULL)
        free(img.data);
}

